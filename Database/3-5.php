<?php
    require_once '3-5_database.php';
    require_once '3-5_validation.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Use database in the applications that you developed</title>
    <style>
        *{
            margin:0;
            padding:0;
        }
        .container{
            display:flex;
            align-items:center;
            justify-content:center;
            height:100vh;
            width:40%;
            margin:auto;
        }
        input{
        width: 100%;
        }
        button{
            width:100%;
            padding:10px;
            margin-top:10px;
            background-color:#007bff;
            cursor:pointer;
            border-radius:15px;
        } 
        .errors{
            color:red;
        }
    </style>
</head>
<body>
    <div class="container">
        <form action="" method="post" enctype="multipart/form-data">
            <div>
                <label>Profile picture:</label>
                <input type="file" name="file" value="<?= $image ?>">
            </div>
            <div>
                <label>First Name:</label>
                <input type="text" name="firstName" value="<?= $firstName ?>">
            </div>
            <div>
                <label>Last Name:</label>
                <input type="text" name="lastName" value="<?= $lastName ?>">
            <div>
                <label>Age:</label>
                <input type="text" name="age" value="<?= $age ?>">
            <div>
                <label>Email:</label>
                <input type="text" name="email" value="<?= $email ?>">
            <div>
            <div>
                <label>Username:</label>
                <input type="text" name="username" value="<?= $username ?>">
            <div>
            <div>
                <label>Password:</label>
                <input type="password" name="password" value="<?= $password ?>">
            <div>
            <p class="errors">
            <?php 
                if ($errors){
                    foreach ($errors as $key => $error) {
                        echo $error.'<br/>';
                    }
                } 
            ?>
            </p>
            <button type="submit" name="submit">Submit</button>
        </form>
    </div>
</body>
</html>