<?php
    require_once '3-5_database.php';    

    $page = $_GET['page'] ?? 1;
    $result_per_page = 10;
    $offset = ($page - 1) * $result_per_page;

    $query = "SELECT first_name, last_name, age, email, image FROM users LIMIT $result_per_page OFFSET $offset";
    $results = $conn->query($query);

    $sql = "SELECT COUNT(*) as total from users ";
    $count_result = $conn->query($sql);

    $total_result = mysqli_fetch_assoc($count_result)['total'];
    $number_of_pages = ceil($total_result / $result_per_page);
?>
<!DOCTYPE html>
<html>
    <head>
        <style>
            table {
                width:100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
                text-align: center;
            }
            #t01 tr:nth-child(even) {
                background-color: #eee;
            }
            #t01 tr:nth-child(odd) {
                background-color: #fff;
            }
            #t01 th {
                background-color: black;
                color: white;
            }
        </style>
    </head>
    <body>
        <h1>User Table</h1> <a href="3-5.php">Back to Home</a> OR <a href="3-5_logout.php">Logout</a>
        <table id="t01">
            <tr>
                <th>Firstname</th>
                <th>Lastname</th> 
                <th>Age</th>
                <th>Email</th>
                <th>Image</th>
            </tr>  
            <tr>
                <?php 
                     if ($results->num_rows > 0) 
                     {
                         while($row = $results->fetch_assoc()) {
                           echo '<tr>';
                           foreach ($row as $key => $result) { 
                               if($key == 'image' && $row['image']){
                                echo "<td><img src=".$row['image']." height=30 width=30 /></td>";
                               } elseif ($key != 'image') {
                                echo '<td>' . $result . '</td>'; 
                               } 
                           }
                           echo '</tr>';
                         }
                     }
                ?>
            </tr>
        </table>
        <h3> Page:
        <?php
            for ($i=1; $i <= $number_of_pages ; $i++) { 
                echo '<a href="3-5_table.php?page='.($i).'">'.$i.'</a>';
            }
        ?>
        </h3>
    </body>
</html>