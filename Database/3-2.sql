CREATE DATABASE database_exercise;

CREATE TABLE `user_profile` (
    user_id int(11) AUTO_INCREMENT PRIMARY KEY,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    age int(11) NOT NULL
  );