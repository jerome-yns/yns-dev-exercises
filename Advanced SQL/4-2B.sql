INSERT INTO 
	`daily_work_shifts`(therapist_id, start_time, target_date, end_time) 
VALUES
	(1, '14:00:00', NOW(),  '15:00:00'),
	(2, '22:00:00', NOW(),  '23:00:00'),
	(3, '00:00:00', NOW(),  '01:00:00'),
	(4, '05:00:00', NOW(),  '05:30:00'),
	(1, '20:00:00', NOW(),  '20:45:00'),
	(5, '05:30:00', NOW(),  '05:50:00'),
	(3, '02:00:00', NOW(),  '02:30:00');
  