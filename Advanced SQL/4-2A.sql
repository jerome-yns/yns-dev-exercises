CREATE DATABASE massage_company;

CREATE TABLE therapists (
    	id int(11) AUTO_INCREMENT PRIMARY KEY,
    	name varchar(255) NOT NULL
    );
    
INSERT INTO 
	`therapists` (name)
VALUES
	('John'),
    ('Arnold'),
    ('Robert'),
    ('Ervin'),
    ('Smith');

CREATE TABLE daily_work_shifts  (
    	id int(11) AUTO_INCREMENT PRIMARY KEY,
    	therapist_id int(11) NOT NULL,
    	target_date date NOT NULL,
    	start_time TIME NOT NULL,
    	end_time TIME NOT NULL
    );
    
