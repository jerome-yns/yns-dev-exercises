SELECT 
	position.*, employee.*, employee_position.*,
(CASE 
    WHEN position.name = "CEO" 
 	    THEN "Chief Executive Officer"
    WHEN position.name = "CTO" 
 	    THEN "Chief Technical Officer"
    WHEN position.name = "CFO" 
 	    THEN "Chief Financial Officer"
    ELSE position.name
END)
	 AS 'Full_position_name'
FROM 
    `employee_positions` employee_position
JOIN  positions position
    ON (employee_position.position_id = position.id)
JOIN  employees employee
    ON (employee_position.employee_id = employee.id);
