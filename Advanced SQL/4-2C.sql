SELECT 
    therapist_id, target_date, start_time, end_time,
CASE WHEN 
    start_time <= '05:59:59' AND start_time >= '00:00:00'
THEN  
    Concat(target_date  + INTERVAL 1 DAY, ' ', start_time)
ELSE  
    Concat(target_date,' ', start_time)
END 
    AS sort_start_time
FROM 
    `daily_work_shifts` 
ORDER BY 
    target_date AND sort_start_time ASC