<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-5 HTML & PHP/title>
</head>
<body>
    <form action="" method="post">
        <input type="date" name="inputDate" value="<?= $inputDate ?>">
        <button type="submit" name="calc">Submit</button>
    </form>
    <?php
        $inputDate = $_POST['inputDate'] ?? '';
        $formatDate = array();
        if (isset($_POST['calc']))
        {
            for ($i = 1; $i <4; $i++) {
              echo date('Y-m-d', strtotime($_POST['inputDate']. '+' . $i . ' day')) .' '.date('l', strtotime($_POST['inputDate']. '+' . $i . ' day')).'<br/>'; 
            }
        }
    ?>
</body>
</html>