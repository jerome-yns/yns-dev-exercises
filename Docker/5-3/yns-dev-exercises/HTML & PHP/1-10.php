<?php
    session_start();

    $image = $_POST['image'] ?? '';
    $firstName = $_POST['firstName'] ?? '';
    $lastName = $_POST['lastName'] ?? '';
    $age = $_POST['age'] ?? '';
    $email = $_POST['email'] ?? '';
    $errors = array();
    $requiredInput = array('firstName', 'lastName', 'age', 'email');
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $targetDIR = 'images/';
        $filepath = $targetDIR . $_FILES["file"]["name"];
        $imageFileType = strtolower(pathinfo($filepath,PATHINFO_EXTENSION));

        foreach ($requiredInput as $key => $input) {
            if (empty($_POST[$input])){
                array_push($errors,ucwords($input) .' is required');
            }
        }
        if ($age && !is_numeric($age)){
            array_push($errors, 'Age should be numeric');
        }

        if ($email && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            array_push($errors, 'Email is not a valid email address');
        }
        
        if (!file_exists('images')){
            mkdir('images');
        }

        if(!ctype_alpha($firstName) || !ctype_alpha($lastName)){
            array_push($errors, 'Name should be consist of Characters only');
        }

        if ($imageFileType && $imageFileType != "jpg" && $imageFileType != "png") {
            array_push($errors, 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
        }

        if (!$errors && move_uploaded_file($_FILES["file"]["tmp_name"], $filepath)) 
        {
            $imageView = $filepath ;
        } 

        if (!$errors){
            $_SESSION['firstName'] = $firstName;
            $_SESSION['lastName'] = $lastName;
            $_SESSION['age'] = $age;
            $_SESSION['email'] = $email;
            $_SESSION['image'] = $imageView;

            header("location:1-10_output.php");
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-10 HTML & PHP</title>
    <style>
        .errors{
            color:red;
        }
    </style>
</head>
<body>
    <form action="1-10.php" method="post" enctype="multipart/form-data">
        <div>
            <label>Profile picture:</label>
            <input type="file" name="file" value="<?= $image ?>">
        </div>
        <div>
            <label>First Name:</label>
            <input type="text" name="firstName" value="<?= $firstName ?>">
        </div>
        <div>
            <label>Last Name:</label>
            <input type="text" name="lastName" value="<?= $lastName ?>">
        <div>
            <label>Age:</label>
            <input type="text" name="age" value="<?= $age ?>">
        <div>
            <label>Email:</label>
            <input type="text" name="email" value="<?= $email ?>">
        <div>
        <p class="errors">
        <?php 
            if ($errors){
                foreach ($errors as $key => $error) {
                    echo $error. '<br/>';
                }
            } 
        ?>
        </p>
        <button type="submit" name="submit">Submit</button>
    </form>
</body>
</html>