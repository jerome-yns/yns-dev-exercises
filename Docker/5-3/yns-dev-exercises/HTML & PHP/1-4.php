<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-4 HTML & PHP</title>
</head>
<body>
    <form action="" method="post">
        <input type="number" name="inputValue">
        <button type="submit" name="submit">Submit</button>
    </form>
    <?php
        $inputValue = $_POST['inputValue'] ?? '';
        if (isset($_POST['submit']))
        {
            for ($i = 1; $i <= $inputValue; $i++)
            {
                if ($i % 15 === 0) {
                    echo 'FizzBuzz <br/>';
                } else if ($i % 3 == 0){
                    echo 'Fizz <br/>';
                } else if ($i % 5 == 0){
                    echo 'Buzz <br/>';
                } else {
                    echo $i.' <br/>';
                }
            }
        }
    ?>
</body>
</html>