<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-2 HTML & PHP</title>
</head>
<body>
    <form action="" method="post">
        <div>
            First Value:<input type="number" name="firstValue" required>
        </div>
        <div>
            Second Value;<input type="number" name="secondValue" required>
        </div>
        <div>
            <button type="submit" name="add">Add</button>
            <button type="submit" name="subtract">Subtract</button>
            <button type="submit" name="multiply">Multiply</button>
            <button type="submit" name="divide">Divide</button>
        </div>
    </form>
    <?php
        $firstValue = $_POST['firstValue'] ?? '';
        $secondValue = $_POST['secondValue'] ?? '';
        $result = 0;
        if (isset($_POST['add'])){
            $result = $firstValue + $secondValue;
        } else if (isset($_POST['subtract'])) {
            $result = $firstValue - $secondValue;
        } else if (isset($_POST['multiply'])) {
            $result = $firstValue * $secondValue; 
        } else if (isset($_POST['divide'])) {
            $secondValue == 0 ? $result = 'Undefined' : $result = $firstValue / $secondValue;
        }   
    ?>
    <br/>Result:<input type="text" value="<?= $result ?>">
</body>
</html>