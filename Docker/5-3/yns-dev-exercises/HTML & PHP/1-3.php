<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-3 HTML & PHP</title>
</head>
<body>
    <form action="" method="post">
        <div>
            First Value:<input type="number" name="firstValue" required>
        </div>
        <div>
            Second Value;<input type="number" name="secondValue" required>
        </div>
        <div>
            <button type="submit" name="submit">Submit</button>
        </div>
    </form>
    <?php
        $firstValue = $_POST['firstValue'] ?? '';
        $secondValue = $_POST['secondValue'] ?? '';
        $gcd = '';

        if (isset($_POST['submit'])){
            for ($i=2; $i<=$firstValue && $i<=$secondValue; $i++)
            {   
                if (($firstValue % $i == 0) && ($secondValue % $i == 0))
                    $gcd = $i;
            }
        }
    ?>
    <br/>GCD:<input type="text" value="<?= $gcd ?>">
</body>
</html>