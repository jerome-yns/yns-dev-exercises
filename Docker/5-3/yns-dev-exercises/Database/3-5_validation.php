<?php 
  session_start();

  $image = $_POST['image'] ?? '';
  $firstName = $_POST['firstName'] ?? '';
  $lastName = $_POST['lastName'] ?? '';
  $age = $_POST['age'] ?? '';
  $email = $_POST['email'] ?? '';
  $username = $_POST['username'] ?? '';
  $password = $_POST['password'] ?? '';
  $errors = array();
  $imageView = '';
  $requiredInput = array('firstName', 'lastName', 'age', 'email', 'username', 'password');
  
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $targetDIR = 'images/';
      $filepath = $targetDIR . $_FILES["file"]["name"];
      $imageFileType = strtolower(pathinfo($filepath,PATHINFO_EXTENSION));

      foreach ($requiredInput as $key => $input) {
          if (empty($_POST[$input])){
              array_push($errors,ucwords($input) .' is required');
          }
      }
      if ($age && !is_numeric($age)){
          array_push($errors, 'Age should be numeric');
      }

      if ($email && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
          array_push($errors, 'Email is not a valid email address');
      }
      
      if (!file_exists('images')){
          mkdir('images');
      }

      if ($username && !preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $username)) {
          array_push($errors, 'Username should only contains alpha numeric characters');
       }

      if(!ctype_alpha($firstName) || !ctype_alpha($lastName)){
          array_push($errors, 'Name should be consist of Characters only');
      }

      if ($imageFileType && $imageFileType != "jpg" && $imageFileType != "png") {
          array_push($errors, 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
      }

      if (!$errors && move_uploaded_file($_FILES["file"]["tmp_name"], $filepath)) 
      {
          $imageView = $filepath ;
      } 

      if (!$errors){
          $_SESSION['firstName'] = $firstName;
          $_SESSION['lastName'] = $lastName;
          $_SESSION['age'] = $age;
          $_SESSION['email'] = $email;
          $_SESSION['image'] = $imageView;
          $_SESSION['username'] = $username;
          $_SESSION['password'] = $password;

          $query = "INSERT INTO users (first_name, last_name, age, email, image, username, password) 
          VALUES('$firstName', '$lastName', '$age', '$email', '$imageView', '$username', '$password')";

            if ($conn->query($query) === TRUE) {
                header("location:3-5_login.php");
            } else {
                echo 'Error:' .$query. '<br>'. $conn->error;
            }
      }
  }
?>