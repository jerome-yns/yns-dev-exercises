SELECT 
    COUNT(middle_name) 
AS 
    count_has_middle 
FROM 
    `employees`
WHERE 
    middle_name <> '';