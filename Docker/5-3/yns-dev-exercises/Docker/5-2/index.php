<?php
$host = "mysql-server";
$user = "root";
$pass = "secret";
$db = "sample";
try {
    $conn = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
    echo "<h1>Connected successfully</h1>";
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>