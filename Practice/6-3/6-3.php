<!DOCTYPE html>
<html lang="en">
<head>
<style>
    table {
    border-collapse: collapse;
    width: 80%;
    margin:auto;
    }
    th, td {
    text-align: left;
    padding: 4px;
    }
    tr:nth-child(even){
        background-color: #f2f2f2
    }
    th {
    background-color: #04AA6D;
    color: white;
    }
    a{
        text-decoration:none;
        color:blue;
    }
    h2{
        text-align:center;
    }
</style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List of exercises</title>
</head>
<body>
<div>
<h2>6-3 Past Exercises</h2>
    <table>
    <tr>
        <th>HTML & PHP</th>
        <th>JavaScript</th>
        <th>Practice</th>
        <th>Database</th>
    </tr>
    <tr>
        <tr>
            <td><a href="./HTML & PHP/1-1.php">1-1 Show Hello World.</a></td>
            <td><a href="./JavaScript/2-1.html">2-1 Show alert.</a></td>
            <td><a href="../6-1/6-1.php">6-1 Multiple choices Quiz</a></td>
            <td><a href="./Database/3-5.php">3-5 Use database in the applications that you developed.s</a></td>
        </tr>
            <td><a href="./HTML & PHP/1-2.php">1-2 The four basic operations of arithmetic.</a></td>
            <td><a href="./JavaScript/2-2.html">2-2 Confirm dialog and redirection</a></td>
            <td><a href="../6-2/6-2.php">6-2 Calendar</a></td>
        <tr>
            <td><a href="./HTML & PHP/1-3.php">1-3 Show the greatest common divisor.</a></td>
            <td><a href="./JavaScript/2-3.html">2-3 The four basic operations of arithmetic</a></td>
        </tr>
            <td><a href="./HTML & PHP/1-4.php">1-4 Solve FizzBuzz problem.</a></td>
            <td><a href="./JavaScript/2-4.html">2-4 Show prime numbers.</a></td>
        <tr>
            <td><a href="./HTML & PHP/1-5.php">1-5 Input date. Then show 3 days from inputted date and its day of the week.</a></td>
            <td><a href="./JavaScript/2-5.html">2-5 Input characters in text box and show it in label.</a></td>
        </tr>
            <td><a href="./HTML & PHP/1-6.php">1-6 Input user information. Then show it in next page.</a></td>
            <td><a href="./JavaScript/2-6.html">2-6 Press button and add a label below button.</a></td>
        <tr>
            <td><a href="./HTML & PHP/1-7.php">1-7 Add validation in the user information form.</a></td>
            <td><a href="./JavaScript/2-7.html">2-7 Show alert when you click an image.</a></td>
        </tr>
            <td><a href="./HTML & PHP/1-8.php">1-8 Store inputted user information into a CSV file.</a></td>
            <td><a href="./JavaScript/2-8.html">2-8 Show alert when you click link.</a></td>
        <tr>
            <td><a href="./HTML & PHP/1-9.php">1-9 Show the user information using table tags.</a></td>
            <td><a href="./JavaScript/2-9.html">2-9 Change text and background color when you press buttons.</a></td>
        </tr>
            <td><a href="./HTML & PHP/1-10.php">1-10 Upload images.</a></td>
            <td><a href="./JavaScript/2-10.html">2-10 Scroll screen when you press buttons.</a></td>
        <tr>  
            <td><a href="./HTML & PHP/1-11.php">1-11 Show uploaded images in the table.</a></td>
            <td><a href="./JavaScript/2-11.html">2-11 Change background color using animation.</a></td>
        </tr>
            <td><a href="./HTML & PHP/1-12.php">1-12 Add pagination in the list page.</a></td>
            <td><a href="./JavaScript/2-12.html">2-12 Show another image when you mouse over an image. Then show the original image when you mouse out.</a></td>
        <tr>
            <td><a href="./HTML & PHP/1-13.php">1-13 Create login form and embed it into the system that you developed</a></td>
            <td><a href="./JavaScript/2-13.html">2-13 Change size of images when you press buttons.</a></td>
        </tr>
            <td></td>
            <td><a href="./JavaScript/2-14.html">2-14 Show images according to the options in combo box.</a></td>
        <tr>
            <td></td>
            <td><a href="./JavaScript/2-15.html">2-15 Show current date and time in real time.</a></td>
        </tr>
        </tr>
    </table>
</div>


</body>
</html>