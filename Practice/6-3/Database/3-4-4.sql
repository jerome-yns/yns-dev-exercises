SELECT 
	employee.last_name as Employee,
    boss.last_name as Boss
FROM
	`employees` employee
JOIN
	employees boss
ON
	employee.boss_id = boss.id