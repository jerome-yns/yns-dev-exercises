SELECT
	first_name,
    middle_name,
    last_name
FROM 
	`employees`
WHERE 
    id IN(SELECT employee_id FROM employee_positions)  
HAVING COUNT(*) > 2
