<?php
    session_start();
    require_once '3-5_database.php';

    $username = $_POST['username'] ?? '';
    $password = $_POST['password'] ?? '';

    $session_username = $_SESSION['username'] ?? '';
    $session_password = $_SESSION['password'] ?? '';
    $errors = array();

    $errors = array();
    $requiredInput = array('username', 'password');
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        foreach ($requiredInput as $key => $input) {
            if (empty($_POST[$input])){
                array_push($errors,ucwords($input) .' is required');
            }
        }
        if (!preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $username)) {
           array_push($errors, 'Username should only contains alpha numeric characters');
        }
        if ($username == $session_username ){
            if (!$errors){
                $_SESSION['username'] = $username;
                $_SESSION['password'] = $password;
    
                header("location:3-5_output.php");
            }
        } else {
            array_push($errors, 'Incorrect username or password');
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>3-5 login</title>
    <style>
        .errors{
            color:red;
        }
    </style>
</head>
<body>
    <form action="3-5_login.php" method="post">
        <input type="text" name="username">
        <input type="password" name="password">
        <p class="errors">
        <?php 
            if ($errors){
                foreach ($errors as $key => $error) {
                    echo $error. '<br/>';
                }
            } 
        ?>
        </p>
        <p><a href="3-5.php">Create Account</a></p>
        <button type="submit" name="submit">Login</button>
    </form>
</body>
</html>

