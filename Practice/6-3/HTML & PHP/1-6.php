<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        *{
            padding:0;
            margin:0;
            box-sizing:border-box;
        }
        .container{
            display:flex;
            align-items:center;
            justify-content:center;
            height:100vh;
            width:40%;
            margin:auto;
        }
        input{
            width: 100%;
            border: 3px solid #555;
        }
        button{
            width:100%;
            padding:10px;
            margin-top:10px;
            background-color:#007bff;
            cursor:pointer;
            border-radius:15px;
        } 
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-6 HTML & PHP</title>
</head>
<body>
    <div class="container">
        <h1>Input user information. Then show it in next page</h1>
        <form action="1-6_output.php" method="post">
            <div>
                <label>First Name:</label>
                <input type="text" name="firstName">
            </div>
            <div>
                <label>Last Name:</label>
                <input type="text" name="lastName">
            <div>
                <label>Age:</label>
                <input type="number" name="age"> 
            <div>
                <label>Email:</label>
                <input type="email" name="email"> 
            <div>
            <button type="submit" name="submit">Submit</button>
        </form>
    </div>
</body>
</html>