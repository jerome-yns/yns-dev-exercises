<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        *{
            padding:0;
            margin:0;
            box-sizing:border-box;
        }
        .container{
            display:flex;
            align-items:center;
            justify-content:center;
            height:100vh;
            width:40%;
            margin:auto;
        }
        input{
            width: 100%;
            border: 3px solid #555;
        }
        button{
            width:100%;
            padding:10px;
            margin-top:10px;
            background-color:#007bff;
            cursor:pointer;
            border-radius:15px;
        } 
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-5 HTML & PHP</title>
</head>
<body>
    <div class="container">
        <form action="" method="post">
            <h1>Show 3 days from inputted date and its day of the week</h1><br/>
            <input type="date" name="inputDate" value="<?= $inputDate ?>">
            <button type="submit" name="calc">Submit</button>
            <?php
                $inputDate = $_POST['inputDate'] ?? '';
                $formatDate = array();
                if (isset($_POST['calc']))
                {
                    for ($i = 1; $i <4; $i++) {
                    echo date('Y-m-d', strtotime($_POST['inputDate']. '+' . $i . ' day')) .' '.date('l', strtotime($_POST['inputDate']. '+' . $i . ' day')).'<br/>'; 
                    }
                }
            ?>
        </form>
    </div>
</body>
</html>