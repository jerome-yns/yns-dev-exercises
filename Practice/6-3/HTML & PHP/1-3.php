<?php
    $firstValue = $_POST['firstValue'] ?? '';
    $secondValue = $_POST['secondValue'] ?? '';
    $gcd = '';

    if (isset($_POST['submit'])){
        for ($i=2; $i<=$firstValue && $i<=$secondValue; $i++)
        {   
            if (($firstValue % $i == 0) && ($secondValue % $i == 0))
            {
                $gcd = $i;
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        *{
            padding:0;
            margin:0;
            box-sizing:border-box;
        }
        .container{
            display:flex;
            align-items:center;
            justify-content:center;
            height:100vh;
            width:40%;
            margin:auto;
        }
        input{
            width: 100%;
            border: 3px solid #555;
        }
        button{
            width:100%;
            padding:10px;
            margin-top:10px;
            background-color:#007bff;
            cursor:pointer;
            border-radius:15px;
        } 
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-3 HTML & PHP</title>
</head>
<body>
    <div class="container">
        <form action="" method="post">
            <h1>Show the greatest common divisor</h1><br/>
            <div>
                First Value:<input type="number" name="firstValue" required>
            </div>
            <div>
                Second Value;<input type="number" name="secondValue" required>
            </div>
            <div>
                <button type="submit" name="submit">Solve</button>
            </div>
            <br/>GCD:<input type="text" value="<?= $gcd ?>">
        </form>
    </div>
</body>
</html>