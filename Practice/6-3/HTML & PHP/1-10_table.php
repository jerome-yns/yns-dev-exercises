<!DOCTYPE html>
<html>
    <head>
        <style>
            table {
                width:100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
                text-align: center;
            }
            #t01 tr:nth-child(even) {
                background-color: #eee;
            }
            #t01 tr:nth-child(odd) {
                background-color: #fff;
            }
            #t01 th {
                background-color: black;
                color: white;
            }
        </style>
        <title>1-10 Table</title>
    </head>
    <body>
        <h1>User Table</h1> <a href="1-10.php">Back to Home</a>
        <table id="t01">
            <tr>
                <th>Firstname</th>
                <th>Lastname</th> 
                <th>Age</th>
                <th>Email</th>
                <th>Image</th>
            </tr>
            <tr>
            <?php
                $csvFile = fopen('./User_Information.csv', 'r');
                while (($csvData = fgetcsv($csvFile)) !== false) {
                        echo '<tr>';
                        for ($i=0; $i < 5 ; $i++) { 
                            if ($i!==4){
                                echo '<td>' .$csvData[$i]. '</td>';
                            } else if (!empty($csvData[4])) {
                                echo "<td><img src=".$csvData[4]." height=50 width=50 /></td>";
                            }
                        }
                        echo "</tr>\n";
                }
                fclose($csvFile);
            ?>
            </tr>
        </table>
    </body>
</html>