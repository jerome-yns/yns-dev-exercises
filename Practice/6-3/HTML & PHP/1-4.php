<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        *{
            padding:0;
            margin:0;
            box-sizing:border-box;
        }
        .container{
            display:flex;
            align-items:center;
            justify-content:center;
            height:100vh;
            width:40%;
            margin:auto;
        }
        input{
            width: 100%;
            border: 3px solid #555;
        }
        button{
            width:100%;
            padding:10px;
            margin-top:10px;
            background-color:#007bff;
            cursor:pointer;
            border-radius:15px;
        } 
        select{
            width:100%;
            margin-top:15px;
        }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-4 HTML & PHP</title>
</head>
<body>
    <div class="container">
        <form action="" method="post">
            <h1>Solve FizzBuzz Problem</h1><br/>
            <input type="number" name="inputValue" placeholder="Input number" required>
            <button type="submit" name="submit">FizzBuzz</button>
            <select name="fizzbuzz">
            <?php
                $inputValue = $_POST['inputValue'] ?? '';
                if (isset($_POST['submit']))
                {
                    for ($i = 1; $i <= $inputValue; $i++)
                    {
                        if ($i % 15 === 0) {
                            echo '<option value="">FizzBuzz</option>';
                        } else if ($i % 3 == 0){
                            echo '<option value="">Fizz <br/></option>';
                        } else if ($i % 5 == 0){
                            echo '<option value="">Buzz <br/></option>';
                        } else {
                            echo '<option value="">'.$i.'</option>';
                        }
                    }
                }
            ?>
          </select>
        </form>
    </div>
</body>
</html>