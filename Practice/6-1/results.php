<?php
    require_once 'database.php';    
    $answers = $_POST['answer'] ?? '' ;
    $username = $_SESSION['username'];
    $score = 0;

    foreach ($answers as $key => $answer) {
        $query = $conn->prepare("SELECT questions,answer from answers JOIN questions ON questions.id = answers.id WHERE answer = ?");
        $query->bind_param('s',$answer);
        $query->execute();
        $results = $query->get_result();


        while ($result = mysqli_fetch_array($results, MYSQLI_ASSOC)) {
            if($result['answer'] == $answer){
                $score += 1 ;
            }
        }
        $query = $conn->prepare("UPDATE users SET score = ? WHERE username = ?");
        $query->bind_param('is',$score,$username);
        $query->execute();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        * {
        margin:0;
        padding:0;
        box-sizing:border-box;
        }
        body{
            text-align:center;
        }
        .container{
            height:100vh;
            display: flex;
            justify-content: center;
            align-items:center;
            flex-direction:column;
        }
        .passed{
            color:#00FF00;
            display:inline-block;
        }
        .failed{
            color:#FF0000;
            display:inline-block;
        }
        .score{
            margin-top:25px;
            border:solid;
            padding:20px;
            width:300px;
            border-radius:5px;
        }
        h2{
            margin-top:15px;
        }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Results</title>
</head>
<body>
<div class="container">
    <h1>Result</h1>
        <?php if($score>=5){
           echo "<h2>Congratulations, you <p class='passed'>passed!</p></h2>";
        } else{
            echo "<h2>Sorry, you <p class='failed'>failed!</p></h2>"; 
        }
        ?>
    <div class="score">
        <h2><?= $score*10 ?>%</h2>
    </div>
 <div>
</body>
</html>