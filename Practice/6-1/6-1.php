<?php 
    require_once 'database.php';   

    $username = $_POST['username'] ?? '';
    $errors = array();
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $query = $conn->prepare("SELECT username FROM users where username = ?"); 
        $query->bind_param('s', $username);
        $query->execute();
        $result = $query->get_result(); 
        $user = $result->fetch_assoc();

        if($user){
            array_push($errors, 'Duplicate text');
        }
        if(!$errors){
            $sql = $conn->prepare("INSERT INTO users (username) VALUES (?)");
            $sql->bind_param('s',$username);
            $sql->execute();

            $_SESSION['username'] = $username;
    
            header("location:quiz.php");
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        * {
            margin:0;
            padding:0;
            box-sizing:border-box;
        }
        body{
            text-align:center;
        }
        .container{
            height:100vh;
            display: flex;
            justify-content: center;
            align-items:center;
            flex-direction:column;
        }
        button{
            padding:10px 20px 10px 20px;
            background-color:#0275d8;
            border-radius:5px;
            font-size:16px;
            color:#ffff;
            cursor:pointer;
            border:none;
        }
        input{
            width: 250px;
            padding: 10px;
            border-radius:10px;
        }
        input:focus{
            outline: none;
        }
        .input-text{
            margin:25px 0;
        }
        .error{
            color:#FF0000;
        }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz</title>
</head>
<body>
    <form action="" method="post">
    <div class="container">
        <h1>Test your knowledge by answering this Quiz</h1>
        <div class="input-text">
            <input type="text" name="username" placeholder="Enter a username" minlength="4" autocomplete="off" required>
            <p class="error">
            <?php 
                if ($errors){
                    foreach ($errors as $key => $error) {
                        echo $error. '<br/>';
                    }
                } 
            ?>
            </p>
        </div>
        <button type="submit">Start Quiz</button>
    </div>
    </form>
</body>
</html>