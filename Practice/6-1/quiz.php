<?php 
    require_once 'database.php';

    $query = "SELECT * FROM questions ORDER BY RAND()";
    $results = $conn->query($query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        * {
            margin:0;
            padding:0;
            box-sizing:border-box;
        }
        body{
            text-align:center;
        }
        .container{
            margin: auto;
            width: 50%;
        }
        .content{
            text-align: left;
            margin-left:30%;
        }
        button{
            text-align:center;
        }
        h1{
            margin-bottom:10px;
        }
        .choices{
            padding-left:12px;
        }
        button{
            padding:10px 20px 10px 20px;
            background-color:#0275d8;
            border-radius:5px;
            font-size:16px;
            color:#ffff;
            cursor:pointer;
            border:none;
        }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>6-1 Docker</title>
</head>
<body>
    <div class="container">
        <h1>Questions</h1>
        <form action="results.php" method="post">
        <div class="content">
            <?php foreach ($results as $key => $result):?>
                <p><?= $key+ 1, '.'. $result['questions'] ?></p>
                <?php for ($i=1; $i < 4 ; $i++): ?>
                    <div class="choices">
                        <input type="radio" name="answer[<?= $key ?>]" value="<?= $result['choice_'.$i] ?>">
                        <label for="<?= $result['choice_'.$i] ?>"><?= $result['choice_'.$i] ?></label><br>
                    </div>
                <?php endfor ; ?><br>
            <?php endforeach ;?>    
        </div>
        <button type="submit">Submit</button>
        </form>
    </div>
</body>
</html>