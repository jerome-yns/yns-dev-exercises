<?php
    if (isset($_GET['date'])) {
        $getDate = $_GET['date'] ?? '';
    } else {
        $getDate = date('Y-m');
    }

    $timestamp = strtotime($getDate . '-01');

    if ($timestamp === false) {
        $getDate = date('Y-m');
        $timestamp = strtotime($getDate . '-01');
    }
    
    $today = date('Y-m-j', time());
    
    $varyDate = date('F Y', $timestamp);

    $dateTimestamp1 = date('Y-m');
    $dateTimestamp2 = date($getDate);

    $prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));
    $next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));

    $days = date('d', $timestamp);
    $months = date('m', $timestamp);
    $years = date('Y', $timestamp);
    $today = strtotime ("now");
    
    $firstDay = mktime(0,0,0,$months,1,$years);
    $daysInMonth = cal_days_in_month(0, $months, $years);
    $dayOfWeek = date('w', $firstDay);

    $currentDay = date('j');
 ?>
<!DOCTYPE html>
<html>
<head>
    <style>
        .container {
            font-family: 'Noto Sans', sans-serif;
            margin-top: 80px;
            display: flex;
            justify-content: center;
            align-items:center;
            flex-direction:column;
        }
        h3 {
            margin-bottom: 30px;
        }
        th {
            height: 30px;
            text-align: center;
        }
        td {
            height: 100px;
        }
        .today {
            background: orange;
        }
        a { 
            text-decoration: none; 
        }
        .date{
            padding:25px;
        }
        #today{
            background-color:green;
        }
    </style>
    <meta charset="utf-8">
    <title>Calendar</title>
</head>
<body>
    <div class="container">
        <form action="" method="post">
            <h3><a href="?date=<?= $prev ?>">&#60;&#60;</a> <?= $varyDate ?> <a href="?date=<?= $next ?>">&#62;&#62;</a></h3>
        </form>
        <table>
            <thead>
                <tr>
                    <th abbr="Sunday" scope="col" title="Sunday">Sun</th>
                    <th abbr="Monday" scope="col" title="Monday">Mon</th>
                    <th abbr="Tuesday" scope="col" title="Tuesday">Tue</th>
                    <th abbr="Wednesday" scope="col" title="Wednesday">Wed</th>
                    <th abbr="Thursday" scope="col" title="Thursday">Thu</th>
                    <th abbr="Friday" scope="col" title="Friday">Fri</th>
                    <th abbr="Saturday" scope="col" title="Saturday">Sat</th>
                </tr>
             </thead>
            <tbody>
                <tr>
                <?php
                    if(0 != $dayOfWeek) { 
                        echo('<td colspan="'.$dayOfWeek.'"> </td>'); 
                    }
                    for($i=1;$i<=$daysInMonth;$i++) {
                        if($i == $currentDay && $dateTimestamp1 === $dateTimestamp2  ) { 
                            echo('<td id="today">'); 
                        } else { 
                            echo("<td>"); 
                        }
                        echo"<div class='date'>".($i)."<div/>";
                        echo("</td>");
                        if(date('w', mktime(0,0,0,$months, $i, $years)) == 6) {
                            echo("</tr><tr>");
                        }
                    }
                ?>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>