<?php
    session_start();
    $firstName = $_SESSION['firstName'];
    $lastName = $_SESSION['lastName'];
    $age = $_SESSION['age'];
    $email = $_SESSION['email'];

    $csvFile = fopen('User_Information.csv', 'a');
    $userInput = array($firstName, $lastName, $age, $email);
    fputcsv($csvFile, $userInput);
    fclose($csvFile);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-9 Output</title>
</head>
<body>
    <h1>User Information</h1>
    <h5>Full Name: <?= $firstName.' '.$lastName ?></h5>
    <h5>Age: <?= $age ?></h5>
    <h5>Email: <?= $email ?></h5>
    <a href="1-9_table.php">Check CSV Table</a>
</body>
</html>