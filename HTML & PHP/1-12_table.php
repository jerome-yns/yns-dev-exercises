<?php
    $table_list = array();
    $csvFile = fopen('./User_Information.csv', 'r');
    while (($csvData = fgetcsv($csvFile)) !== false) {
        array_push($table_list,$csvData);   
    }
    fclose($csvFile);
    $page = $_GET['page'] ?? 1;
    $result_per_page = 10;
    $total = count($table_list);
    $offset = ($page - 1) * $result_per_page;
    $number_of_pages = ceil($total / $result_per_page);
    $results = array_slice($table_list,$offset,$result_per_page);
?>
<!DOCTYPE html>
<html>
    <head>
        <style>
            table {
                width:100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
                text-align: center;
            }
            #t01 tr:nth-child(even) {
                background-color: #eee;
            }
            #t01 tr:nth-child(odd) {
                background-color: #fff;
            }
            #t01 th {
                background-color: black;
                color: white;
            }
        </style>
        <title>1-12 Table</title>
    </head>
    <body>
        <h1>User Table</h1> <a href="1-12.php">Back to Home</a>
        <table id="t01">
            <tr>
                <th>Firstname</th>
                <th>Lastname</th> 
                <th>Age</th>
                <th>Email</th>
                <th>Image</th>
            </tr>  
                <?php 
                    foreach ($results as $key => $list) {
                        echo '<tr>';
                        for ($i=0; $i < 5 ; $i++) { 
                            if ($i!==4){
                                echo '<td>' .$list[$i]. '</td>';
                            } else if ($list[4]) {
                                echo "<td><img src=".$list[4]." height=30 width=30 /></td>";
                            }
                        } 
                        echo '</tr>';
                    }
                ?>
            </tr>
        </table>
        <h3> Page:
        <?php
            for ($i=1; $i <= $number_of_pages ; $i++) { 
                echo '<a href="1-12_table.php?page='.($i).'">'.$i.'</a>';
            }
        ?>
        </h3>
    </body>
</html>