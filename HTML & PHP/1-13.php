<?php
    session_start();
    $image = $_POST['image'] ?? '';
    $firstName = $_POST['firstName'] ?? '';
    $lastName = $_POST['lastName'] ?? '';
    $age = $_POST['age'] ?? '';
    $email = $_POST['email'] ?? '';
    $username = $_POST['username'] ?? '';
    $password = $_POST['password'] ?? '';
    $errors = array();
    $requiredInput = array('firstName', 'lastName', 'age', 'email', 'username', 'password');
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $targetDIR = 'images/';
        $filepath = $targetDIR . $_FILES["file"]["name"];
        $imageFileType = strtolower(pathinfo($filepath,PATHINFO_EXTENSION));

        foreach ($requiredInput as $key => $input) {
            if (empty($_POST[$input])){
                array_push($errors,ucwords($input) .' is required');
            }
        }
        if ($age && !is_numeric($age)){
            array_push($errors, 'Age should be numeric');
        }

        if ($email && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            array_push($errors, 'Email is not a valid email address');
        }
        
        if (!file_exists('images')){
            mkdir('images');
        }

        if ($username && !preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $username)) {
            array_push($errors, 'Username should only contains alpha numeric characters');
         }

        if(!ctype_alpha($firstName) || !ctype_alpha($lastName)){
            array_push($errors, 'Name should be consist of Characters only');
        }

        if ($imageFileType && $imageFileType != "jpg" && $imageFileType != "png") {
            array_push($errors, 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
        }

        if (!$errors && move_uploaded_file($_FILES["file"]["tmp_name"], $filepath)) 
        {
            $imageView = $filepath ;
        } 

        if (!$errors){
            $_SESSION['firstName'] = $firstName;
            $_SESSION['lastName'] = $lastName;
            $_SESSION['age'] = $age;
            $_SESSION['email'] = $email;
            $_SESSION['image'] = $imageView;
            $_SESSION['username'] = $username;
            $_SESSION['password'] = $password;

            header("location:1-13_login.php");
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-13 HTML & PHP</title>
    <style>
         *{
            margin:0;
            padding:0;
        }
        .container{
            display:flex;
            align-items:center;
            justify-content:center;
            height:100vh;
            width:40%;
            margin:auto;
        }
        input{
            width: 100%;
        }
        button{
            width:100%;
            padding:10px;
            margin-top:10px;
            background-color:#007bff;
            cursor:pointer;
            border-radius:15px;
        } 
        .errors{
            color:red;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Create login form and embed it into the system that you developed</h1>
        <form action="1-13.php" method="post" enctype="multipart/form-data">
            <div>
                <label>Profile picture:</label>
                <input type="file" name="file" value="<?= $image ?>">
            </div>
            <div>
                <label>First Name:</label>
                <input type="text" name="firstName" value="<?= $firstName ?>">
            </div>
            <div>
                <label>Last Name:</label>
                <input type="text" name="lastName" value="<?= $lastName ?>">
            <div>
                <label>Age:</label>
                <input type="text" name="age" value="<?= $age ?>">
            <div>
                <label>Email:</label>
                <input type="text" name="email" value="<?= $email ?>">
            <div>
            <div>
                <label>Username:</label>
                <input type="text" name="username" value="<?= $username ?>">
            <div>
            <div>
                <label>Password:</label>
                <input type="password" name="password" value="<?= $password ?>">
            <div>
            <p class="errors">
            <?php 
                if ($errors){
                    foreach ($errors as $key => $error) {
                        echo $error. '<br/>';
                    }
                } 
            ?>
            </p>
            <button type="submit" name="submit">Submit</button>
        </form>
    </div>
</body>
</html>