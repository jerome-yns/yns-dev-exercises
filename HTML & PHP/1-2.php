<?php
    $firstValue = $_POST['firstValue'] ?? '';
    $secondValue = $_POST['secondValue'] ?? '';
    $result = 0;
    if (isset($_POST['add'])){
        $result = $firstValue + $secondValue;
    } else if (isset($_POST['subtract'])) {
        $result = $firstValue - $secondValue;
    } else if (isset($_POST['multiply'])) {
        $result = $firstValue * $secondValue; 
    } else if (isset($_POST['divide'])) {
        $secondValue == 0 ? $result = 'Undefined' : $result = $firstValue / $secondValue;
    }   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        *{
            padding:0;
            margin:0;
            box-sizing:border-box;
        }
        .container{
            display:flex;
            align-items:center;
            justify-content:center;
            height:100vh;
            width:40%;
            margin:auto;
        }
        input{
            width: 100%;
            border: 3px solid #555;
        }
        .operators-btn{
            padding:15px 0px;
        }
        button{
            padding:10px;
            background-color:#007bff;
            cursor:pointer;
            border-radius:15px;
        } 
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-2 HTML & PHP</title>
</head>
<body>
    <div class="container">
        <form action="" method="post">
            <div>
                <h1>The four basic operations of arithmetic</h1><br/>
                <p>First Value</p>
                <input type="number" name="firstValue" required>
            </div>
            <div>
                <p>Second Value</p>
                <input type="number" name="secondValue" required>
            </div>
            <div class="operators-btn">
                <button type="submit" name="add">Add</button>
                <button type="submit" name="subtract">Subtract</button>
                <button type="submit" name="multiply">Multiply</button>
                <button type="submit" name="divide">Divide</button>
            </div>
            <br/>Result:<input type="text" value="<?= $result ?>">
        </form>
    </div>
</body>
</html>