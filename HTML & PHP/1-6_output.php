<?php
    $firstName = $_POST['firstName'] ?? '';
    $lastName = $_POST['lastName'] ?? '';
    $age = $_POST['age'] ?? '';
    $email = $_POST['email'] ?? '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-6 Output</title>
</head>
<body>
    <h1>User Information</h1>
    <h5>Full Name: <?= $firstName.' '.$lastName ?></h5>
    <h5>Age: <?= $age ?></h5>
    <h5>Email: <?= $email ?></h5>
</body>
</html>